**Frank da Printer (Martin’s Monster) – User Manual** 

A Living document updated 10 May 2022

**Required Demo files on SD card “Frank Notes”**

**Summary** – The following describes the steps that a new user (Dale), who knows nothing about 3D printing hardware or software, used to create his first piece of extruded plastic under Martin’s guidance. Some details should probably be filled in by somebody that actually knows what they are doing. 

**1) Find or Create a .STL** (3D shape file) that describes the surface geometry of the finished object, noting some constraints, e.g.:

- filament thickness

- tractable geometry (sacrificial overhang supports, etc)  

**2) Convert the .STL to gcode that Frank can work with.**

Install slicer software 

(Windows Standalone 2.3.3 – default installation tested, options “all printers”, “simple”) 

https://www.prusa3d.com/prusaslicer/ 

with the configuration file:

PrusaSlicer_Frank_config_bundle_DK1.ini 

- Martin’s original file with the fan speed slowed down to 30% to prevent thermal runaway

PrusaSlicer_Frank_config_bundle_MartinOriginal.ini 

– Martin’s original file in case Dale inadvertently changed something important

Note options for:

- slice thickness

- infill density

- filament type (PLA used in test) – 1.75mm (long time out of sealed package)

Support structures automatic creation was shutoff (only slightly relevant due to bevel on bottom surface) 

hit <create slices> to create .gcode file

- can insert pause at specific slice (e.g. to change filament colour or embed a nut, etc)

**3) Use SD card to transfer gcode to Frank**

example file “MartinTuningWrench_0.25mm_PLA_14m.gcode”

**4) Hardware set-up:**

- filament installation (PLA very strong and forgiving of ambient temperature and moisture absorption, plus biodegradable if one has a high temperature industrial composter)

- power up – warm base (probably auto-timed?)

- can switch filament mid-print (do not pause first) – using extract/insert filament options.

**5) Printer options - press knob**

> MOTION

> AUTO Z-ALIGN (automated printbed levelling in 1 dimension)

> PRINT FROM MEDIA (files sorted by timestamp)

> PRINT

> TUNE

> SET PROBE Z-OFFSET 

-from 1.35 to 1.30 (or maybe -1.35 to -1.30) to fix initial layer rippling (did not figure out how to set this in prusaslicer config file)

**6) Remove finished object**

- remove magnetic plate and cool down printbed

- flex (or use metal scraper) to remove

**7) Power off after temperature drops below 70C (on printer screen)**

**Maintenance?**

- confirm that cooling fans are running

**Frank’s (Marlin) firmware** (Craig M noted that these components are required) 

- Unified Bed Levelling

- Linear Advance

- Baby Steps

- G34

**Thermal Runaway** 

- safety feature shutdown (with corresponding loud beep that cannot be prevented by turning the sound off in the menu). Error occurs when temperature reading drops (or perhaps increases as well?) unexpectedly. Fire risk arises if there is a thermistor failure, because feedback mechanism will keep pumping electricity into hotend until it overheats. 

- Only experienced on largest (90+ minutes) test prints when “Fan 1” was set to 100%, no problem at 30%
